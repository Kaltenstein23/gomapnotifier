import requests
from requests.exceptions import ConnectionError as ConnError
import time
import json
from pathlib import Path
from datetime import datetime, timedelta
from pokedex import Pokemon
import googlemaps as maps

class DiscordWebhook(object):
	'''Ein Endpunkt für den Webhook'''
	def __init__(self,url,username):
		# An welche URL geht der Post-Request?
		self.endpoint = url
		# Der GMaps-Key (für das Reverse Geocoding) - Woher kriegt ihr einen? -> https://github.com/googlemaps/google-maps-services-python
		self.gmaps = maps.Client(key='getyourown')
		# Unter welchem Namen soll der Post erscheinen?
		self.username = username
		# Welche Pokemon ID sollen von den Meldungen ausgeschlossen werden?
		self.excludes = [129,218,302,216,93,94,92,10,11,12,13,14,15,16,17,18,19,20,21,22,23,29,30,32,33,35,39,40,41,43,44,46,47,48,49,52,54,55,60,61,62,69,70,72,73,77,78,79,80,86,87,90,91,96,97,98,104,109,116,118,119,120,121,122,124,125,126,132,133,161,162,163,164,165,166,167,168,170,175,176,177,178,183,184,185,187,188,189,190,193,194,198,199,202,203,206,207,211,213,220,223,224,226,227,234]

	
	def send(self,e_id,pokemon_id,lat,lon,despawn):
		p = Pokemon(pokemon_id)
		# Ist das Pokémon in der Exclude-Liste?
		if pokemon_id not in self.excludes:
			# Suche die Adresse via Reverse Geocoding.
			loc = self.gmaps.reverse_geocode((lat,lon),result_type='street_address',language='de')
			# Titel für den Webhook vorbereiten
			title = "Ein wildes {0} ist aufgetaucht".format(p.name)
			# Haben wir eine Adresse?
			if loc:
				# Ja, bauen wir den Adress-String
				location = loc[0]['formatted_address'].split(',')[0]
			else:
				# Nein, Platzhalter rein...
				location = 'Maps-Link'
			# Header für den Request vorbereiten
			header = { 'User-Agent':'Kaltenstein-GoMap-Notifier','Content-Type':'application/json'}
			# Restspawnzeit berechnen
			time_left = datetime.fromtimestamp(int(despawn)) - datetime.now()
			# JSON-Payload für den Request vorbereiten - Details findet ihr unter https://discordapp.com/developers/docs/resources/webhook
			body = json.dumps({"username":self.username,"content":title,"embeds":[{"type":"rich","description":"[{0}](https://maps.google.de/maps?q={1},{2}) \nDespawn: {3} \nNoch **{4}** aktiv".format(location,lat,lon,datetime.fromtimestamp(int(despawn)).strftime('%H:%M:%S'),datetime.fromtimestamp(time_left.seconds).strftime('%M:%S')),"thumbnail":{"url":"http://vflwaldi.dyndns.org:1946/sprites/{0}.png".format(pokemon_id),"width":"96","height":"96"}}]})
			# Spawns, die kürzer als 3 Minuten laufen, werden ausgeschlossen.
			if datetime.fromtimestamp(int(despawn)) > (datetime.now() + timedelta(minutes=3)):
				r = requests.post(self.endpoint,data=body,headers=header)
				print("{0} (e_id: {1}) an Discord gesendet.".format(p.name,e_id))
			else:
				print("{0} (e_id: {1}) ignoriert - zu wenig Zeit.".format(p.name,e_id))
		else:
			print("{0} (e_id: {1}) ignoriert - in Exclude-Liste.".format(p.name,e_id))

class RaidDiscordWebhook(object):
	'''Ein Endpunkt für den Webhook'''
	def __init__(self,url,username):
		# An welche URL geht der Post-Request?
		self.endpoint = url
		# Unter welchem Namen soll der Post erscheinen?
		self.username = username
	
	def send(self,name,lvl,gym_id,lat,lon,start,end,pokemon_id=None):
		# Suche die Adresse via Reverse Geocoding.
		loc = self.gmaps.reverse_geocode((lat,lon),result_type='street_address',language='de')
		# Titel für den Webhook vorbereiten
		if pokemon_id is not None:
			p = Pokemon(pokemon_id)
			title = "Ein {0}-Raid ist geschlüpft - **ID** {1}".format(p.name,pokemon_id)
		else:
			title = "Ein Level {0}-Raid schlüpft bald".format(lvl)
		# Header für den Request vorbereiten
		header = { 'User-Agent':'Kaltenstein-GoMap-Notifier','Content-Type':'application/json'}
		# Restspawnzeit berechnen
		if pokemon_id is None:
			time_left = datetime.fromtimestamp(int(start)) - datetime.now()
			if lvl == 5:
				thumburl = 'https://raw.githubusercontent.com/ZeChrales/PogoAssets/master/static_assets/png/ic_raid_egg_legendary.png'
			elif lvl == 4 or lvl == 3:
				thumburl = 'https://raw.githubusercontent.com/ZeChrales/PogoAssets/master/static_assets/png/ic_raid_egg_rare.png'
			elif lvl == 2 or lvl == 1:
				thumburl = 'https://raw.githubusercontent.com/ZeChrales/PogoAssets/master/static_assets/png/ic_raid_egg_normal.png'
			body = json.dumps({"username":self.username,"content":title,"embeds":[{"type":"rich","description":"[{0}](https://maps.google.de/maps?q={1},{2}) \nDespawn: {3} \nNoch **{4}** bis zum Spawn".format(location,lat,lon,datetime.fromtimestamp(int(start)).strftime('%H:%M:%S'),datetime.fromtimestamp(time_left.seconds).strftime('%M:%S')),"thumbnail":{"url":thumburl,"width":"96","height":"96"}}]})
		else:
			body = json.dumps({"username":self.username,"content":title,"embeds":[{"type":"rich","description":"[{0}](https://maps.google.de/maps?q={1},{2}) \nDespawn: {3} \nNoch **{4}** aktiv".format(name,lat,lon,datetime.fromtimestamp(int(end)).strftime('%H:%M:%S'),datetime.fromtimestamp(time_left.seconds).strftime('%M:%S')),"thumbnail":{"url":"http://vflwaldi.dyndns.org:1946/sprites/{0}.png".format(pokemon_id),"width":"96","height":"96"}}]})
		# Spawns, die kürzer als 3 Minuten laufen, werden ausgeschlossen.
		if datetime.fromtimestamp(int(despawn)) > (datetime.now() + timedelta(minutes=3)):
			r = requests.post(self.endpoint,data=body,headers=header)

if __name__ == "__main__":
	# Scanner-Break-Variable - Soll der Scanner an irgendeinem Zeitpunkt aufhören zu laufen, setzt die Variable im Code auf False
	running = True
	# Pokémon anzeigen
	scan_mon = True
	# Raids anzeigen - Noch nicht implementiert...
	scan_raids = True
	# Raid-Eier anzeigen - Noch nicht implementiert
	ready_raids = False
	last_eid = 0
	last_gid = 0
	raid_reported = list()
	ready_reported = list()
	if scan_mon is True:
		# Es muss mindestens EIN Endpunkt bereitstehen.
		poke_webhook = DiscordWebhook('getyourown','PokéSpotter')
		raid_webhook = RaidDiscordWebhook('https://getyourown','RaidSpotter')
	while running is True:
		try:
			# Bereite die Parameter vor (ex = urlencoded Liste der Excludes, n,s,w,e GPS-Koordinaten Punkte des anzuzeigenden Rechtecks, last_eid = die zuletzt angezeigte encounter ID, last_gid = die zuletzt angezeigte Gym-ID)
			params = {'ex':'%5B302%2C353%2C355%2C84%2C63%2C81%2C216%2C129%2C218%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19%2C20%2C21%2C22%2C23%2C29%2C30%2C32%2C33%2C35%2C39%2C215%2C216%2C213%2C40%2C41%2C43%2C44%2C46%2C47%2C48%2C49%2C52%2C54%2C55%2C60%2C61%2C62%2C69%2C70%2C72%2C73%2C77%2C78%2C79%2C80%2C86%2C87%2C90%2C91%2C96%2C97%2C98%2C104%2C109%2C116%2C118%2C119%2C120%2C121%2C122%2C124%2C125%2C126%2C132%2C133%2C161%2C162%2C163%2C164%2C165%2C166%2C167%2C168%2C170%2C175%2C176%2C177%2C178%2C183%2C184%2C185%2C187%2C188%2C189%2C190%2C193%2C194%2C198%2C199%2C202%2C203%2C206%2C207%2C211%2C220%2C223%2C224%2C226%2C227%2C234%5D','w':'9.827785491943358','e':'9.885635375976562','n':'52.255917238088095','s':'52.22012305024242','mid':str(last_eid),'gid':0}
			r = requests.post('https://mapdata2.gomap.eu/mnew.php',params=params)
			items = r.json()
			if scan_mon is True:
				mons = items['pokemons']
				for item in mons:
					if last_eid < item['eid']:
						last_eid = item['eid']
					poke_webhook.send(item['eid'],item['pokemon_id'],item['latitude'],item['longitude'],item['disappear_time'])
			# Not Ready for deployment - TODO - Raidanzeige fixen - Mehrfachposts lösen...
			if scan_raids is True:
				gyms = items['gyms']
				for item in gyms:
					if 'lvl' in item.keys():
						if 'rcp' not in item.keys() and ready_raids is True:
							if item['gym_id'] not in ready_reported:
								if item['lvl'] == 3:
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'])
									ready_reported.append(item['gym_id'])
								elif item['lvl'] == 4:
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'])
									ready_reported.append(item['gym_id'])
								elif item['lvl'] == 5:
									ready_reported.append(item['gym_id'])
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'])
								else:								
									print(f"Raid-Level zu niedrig - Melde Raid an {item['name']}(Gym-ID:{item['gym_id']}) nicht an Discord.")
								ready_reported.append(item['gym_id'])
							else:
								print(f"Raid bereits gemeldet.")
						elif 'rcp' in item.keys():
							if item['gym_id'] not in raid_reported:
								try:
									ready_reported.remove(item['gym_id'])
								except ValueError:
									print(f"Failed removal of Gym {item['gym_id']} from ready_reported, item not found...")
								if item['lvl'] == 3:
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'],item['rpid'])
									raid_reported.append(item['gym_id'])
								elif item['lvl'] == 4:
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'],item['rpid'])
									raid_reported.append(item['gym_id'])
								elif item['lvl'] == 5:
									raid_webhook.send(item['name'],item['lvl'],item['gym_id'],item['latitude'],item['longitude'],item['rs'],item['re'],item['rpid'])
									raid_reported.append(item['gym_id'])
								else:
									print(f"Raid-Level zu niedrig - Melde Raid an {item['name']} nicht an Discord.")
							else:
								print(f"Raid bereits gemeldet.")
					else:
						if item['gym_id'] in ready_reported:
							try:
								ready_reported.remove(item['gym_id'])
								print(f"Removed {item['gym_id']} from reported eggspawns")
							except ValueError:
								print(f"{item['gym_id']} is not in reported eggspawns")
						if item['gym_id'] in raid_reported:
							try:
								raid_reported.remove(item['gym_id'])
								print(f"{item['gym_id']} removed from reported raid spawns.")
							except ValueError:
								print(f"{item['gym_id']} not in reported raid spawns.")
						print(f"Kein Raid an {item['name']}")
			time.sleep(45)
		except ConnError:
			print("Connection to gomap.eu failed - waiting for a minute till retry...")
			time.sleep(60)
		except maps.exceptions.TransportError:
			pass
		except KeyboardInterrupt:
			with open('./last_eid','w') as fp:
				fp.write(str(last_eid))
				fp.close()
			with open('./last_gid','w') as fp:
				fp.write(str(last_gid))
				fp.close()
			running = False
