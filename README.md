# GoMapNotifier #

Ein Python-Script, dass euch die Pokémon-Spawns (evtl. auch Raids) auf eurem Server anzeigt.

### Setup ###

* Repo klonen
* Daten anpassen (params)
* Requirements installieren (pip install -r requirements.txt)

### Kann ich helfen? ###

Wenn du Python kannst, schau dir den [Issue-Tracker](https://bitbucket.org/Kaltenstein23/gomapnotifier/issues?status=new&status=open) an. 
Entwicklung des Scripts finden auf dem dev-Branch statt. 
Pull Requests werden nicht auf dem Master-Branch akzeptiert.